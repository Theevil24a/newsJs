const HOST = location.host,
      PATH_DATA = "collections/news.json";

var url = ''
    elements = [];

if (HOST === "localhost"){
    url += HOST+'/nelsonpalacio/'+PATH_DATA
}

function string_new(_id,description,title,next,before,image){
    return ` 
        <div class="mdl-cell--12-col">
            <a data-before="`+before+`" data-id="`+_id+`" class="before"><img src="images/arrowleft.png"/>Anterior</a>
            <a data-next="`+next+`" data-id="`+_id+`" class="next">Siguiente<img src="images/arrowrigth.png"/></a>
        </div>
        <div class="mdl-card__media mdl-color-text--grey-50">
            <h3><a href="entry.html">`+title+`</a></h3>
        </div>
        <div class="mdl-color-text--grey-600 mdl-card__supporting-text">
            `+description+`
        </div>
        <div class="mdl-color-text--grey-600 mdl-card__supporting-text image_`+_id+`">
            <img style='width: 96.5%;' src='`+image+`'/>
        </div>`
}

function appendtohtml(_id,description,title,next,before,image){
    image = image[0]
    $('.news').html("")
    $(".news").append(string_new(_id,description,title,next,before,image))
}

function instance_element_clouster(_id, value){
    next = _id + 1
    before = _id - 1
    id_image = 0

    if (before < 0){
        before = ''
    }
    
    appendtohtml(_id, value.descripcion, value.title, next, before, value.images)

    setInterval( () => {
        $(".image_"+_id).html("")
        $(".image_"+_id).append("<img style='width: 96.5%;' src='"+value.images[id_image]+"'/>")
        
        id_image += 1

        if (id_image == value.images.length){
            id_image = 0
        }
    },6000)
    
    $(".next").click(e => {
        e.preventDefault()
        key = $('.next').data('next')
        instance_element_clouster(key ,elements[key])
    })
    
    $(".before").click(e => {
        e.preventDefault()
        key = $('.before').data('before')
        instance_element_clouster(key ,elements[key])
    })
}


$(document).ready(()=>{
    $.ajax({
        url: PATH_DATA,
    }).done(data => {
        $.each(data, (key, value) =>{
            elements.push(value)

            if (key == 0){
                instance_element_clouster(key, value)
            }
            
        })
    })

})